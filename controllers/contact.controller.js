const contactController = {
    getFormPage : (req, res) => {
        // db.contect()
        // res.writeHead(200)
        // res.end("Formulaire de contact")
        res.status(200).render('contact')
    },
    getById : (req, res) => {
        //Pour récupérer l'id passé en paramètre
        const contactId = req.params.id
        res.writeHead(200)
        res.end(`Contact dont l'id est ${contactId}`)

    },
    postForm : (req, res) => {
        console.log("Formulaire soumis !");
        console.log(req.body)
        //TODO ajouter en db
        res.status(201).redirect('/')
    },
    getListPage : (req, res) => {}
}

module.exports = contactController