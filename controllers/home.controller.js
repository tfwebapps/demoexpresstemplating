const homeController = {
    getPage : (req, res) => {
        // res.writeHead(200)
        // res.end("Bienvenue sur la Home Page 🏚")
        // res.status(200).send("<h1>Bienvenue sur la Home Page 🏚</h1>")
        const data = {
            today : new Date().toLocaleDateString('fr-BE', { dateStyle : 'full'}),
            trainers : [] 
        }
        res.status(200).render('home', data)

    },
    notFoundPage : (req, res) => {
        // res.writeHead(200)
        // res.end("Erreur 404 - Not Found 🤖")
        // res.status(404).json({ error : "bipboup", message : '404 Not Found' })
        res.status(404).render('not-found')
    },
    getGoogle : (req, res) => {
        res.redirect('https://google.be')
        //res.redirect('/contact') -> Pratique aussi pour faire des redirections internes
    }
}

module.exports = homeController