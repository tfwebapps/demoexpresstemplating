## Créer le projet
- npm init

## Installer Nodemon
- npm i nodemon --save-dev → Permettre au programme de tourner non stop en mode dev

## Installer Express
- npm i express

## Installer DotEnv pour setup les variables d'environnement
- npm i dotenv

## Installer ejs (ou pug allez)
- npm i ejs (pug)