require("dotenv").config() // → Récupère tout ce qui est déclaré dans les fichiers .env et les mets dans les variables d'environnement
const { PORT } = process.env

// * Etape 1 - Importer express (pas oublier de l'installer avant)
const express = require("express")

const morgan = require("morgan")

// * Etape 2 - Créer le serveur (wow plus rapide que http)
const app = express()

// ---- STATIC FILES ---- //
//Pour aller lire les fichiers html/images/etc dans le dossier public
app.use(express.static('public'))
// ---- ------------ ---- //

// ---- MIDDLEWARES POUR POUVOIR RECEVOIR LES DONNEES DU FORM ---- //
app.use(express.urlencoded({ extended : true }))
app.use(express.json())
// ---- ----------------------------------------------------- ---- //

// * MiddleWare APP Level !
// ! Attention, à positionner après la création du serveur express
//Mais avant utilisation des routes
//APP level -> Va se déclencher pour chacune des requêtes (pour toute l'app), elle est globale à toute l'application
app.use((req, res, next) => {
    //req -> la requête interceptée
    //res -> la response
    //next -> la méthode pour passer à la suite
    console.log(`Une nouvelle requête sur l'url : ${req.url} a été faite à ${new Date().toLocaleTimeString()}`);
    next() //Indique qu'il faut qu'elle continue son chemin, sinon, ça va patiner dans la semoule
})

app.use(morgan('tiny'))

// ---- Templating ---- //
app.set('view engine', 'ejs') // -> Pour préciser pour que le moteur de vues sera ejs (ou pug)
app.set('views', 'views' /* nom dossier */) // -> Pour préciser où se trouve le dossier avec les vues
// ---- ---------- ---- //


//#region Avant rangement des routes dans un dossier dédié
// app.get('/', () => {
//     console.log("Page d'accueil !");
// })
// app.get('/contact', () => {
//     console.log("Page contact : Voici le formulaire");
// })
// app.post('/contact', () => {
//     console.log("Page contact : Envoi du formulaire");
// })
// app.get('**', () => {
//     console.log("404 - Not Found");
// })
//#endregion

//Import du router
const router = require("./routes") //On require tout le dossier routes et c'est ce qui est exporté dans le fichier index.js qui va être importé (attention si pas de index.js vous devrez préciser le fichier)

//Utilisation router dans l'app
app.use(router)

// * ERROR HANDLER MIDDLEWARE 
// ! ATTENTION toujours à la fin (après création de l'app et utilisation des routes)
app.use((err, req, res, next) => {
    console.log("Une erreur est survenue");
    console.log(err);
    
    if(res.headerSent) {
        next(err)
    }

    res.status(500).send({ error : err.toString()})
})

// * Etape 3 - Lancer/Ecouter le serveur (bon ça c'est pareil)
app.listen(PORT, () => {
    console.log(`Express Server Started on port : ${ PORT }`);
})