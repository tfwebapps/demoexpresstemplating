//Création d'un router pour contact
const contactRouter = require("express").Router()

const contactController = require("../controllers/contact.controller")

//Configuration de toutes les routes possibles pour contact
contactRouter.get('/', contactController.getFormPage)
contactRouter.get('/:id([0-9]{1,3})', contactController.getById)
//:id -> Segment dynamique, va contenir un id
//ex : /contact/45
//:id([0-9]{1,3}) -> Ne va autoristé que les id entre 0 et 999, en dehors, route inconnues donc -> 404 not found
contactRouter.post('/',contactController.postForm)
contactRouter.get('/list', contactController.getListPage)



//export de ce router
module.exports = contactRouter