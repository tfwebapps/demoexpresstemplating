const router = require("express").Router()

const contactRouter = require('./contact.router')

const homeController = require("../controllers/home.controller")

router.get('/', homeController.getPage)
router.get('/google', homeController.getGoogle)
router.use('/contact', contactRouter)

//Attention toujours à la fin du index puisqu'il signigie "Si aucun chemin au dessus"
router.get('*', homeController.notFoundPage)


module.exports = router